package com.company;

import java.io.InputStreamReader;
import java.io.*;
import java.lang.StringBuilder;
import java.util.*;
import java.io.IOException;
public class Main
{

    public static void main(String[] args) {
        InputStreamReader reader = null;
        TreeMap<String, Integer > CSVtab = new TreeMap<String, Integer>();
        try {

            for(String s : args)
                System.out.println(s);

            reader = new InputStreamReader(new FileInputStream(args[0]));
            System.setOut(new PrintStream (new FileOutputStream ( "output.csv" ))); // создали файл и направил вывод туда


            int allCount =0;
            int i;
            StringBuilder word = new StringBuilder();

            while((i = reader.read()) != -1)
            {
                char c = (char)i;
                boolean b = Character.isLetterOrDigit(c);
                if(b)
                    word.append(c);
                else
                {

                    int count =1;
                    if(CSVtab.containsKey(word.toString()))
                        count =CSVtab.get(word.toString())+1;
                    if (word.length()!=0)
                    {
                        allCount = allCount+1;
                        CSVtab.put(word.toString(),count);
                        word.setLength(0);
                    }

                }
            }

            CSVtab.put("vspm",allCount);

            CSVtab.entrySet().stream()
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .forEach((k)->System.out.format( ((k.getKey()!="vspm")? ( "%s; "+ "%d; "+ "%.3f%%" + "\n"  ):("Word; Count; Procent \n")), k.getKey(), k.getValue(), (double)k.getValue()/CSVtab.get("vspm")*100 ));


//read the data here
        } catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        } finally {
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }
    }
}
