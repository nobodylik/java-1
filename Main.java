package com.company;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.*;
import java.lang.StringBuilder;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
public class Main
{

    public static void main(String[] args) {
        OutputStreamWriter writer = null;
        InputStreamReader reader = null;
       // TreeSet<String> CSVtab = new TreeSet<>();
        //Map<String, Integer> CSVtab = new HashMap<String, Integer>();
        TreeMap<String, Integer> CSVtab = new TreeMap<String, Integer>();
        try {
            reader = new InputStreamReader(new FileInputStream("input.txt"));
            //writer = new FileWriter("output.txt");
            writer = new OutputStreamWriter(new FileOutputStream("output.txt"));




            int i;
            StringBuilder word = new StringBuilder();


            while((i = reader.read()) != -1)
            {
                char c = (char)i;
                boolean b = Character.isLetterOrDigit(c);
                if(b)
                    word.append(c);
                else
                {

                    int count =1;
               //     int f = CSVtab. get(word.toString());
                    if(CSVtab.containsKey(word.toString()))
                        count =CSVtab.get(word.toString())+1;
                    if (word.length()!=0)
                    {
                        CSVtab.put(word.toString(),count);
                        word.setLength(0);
                    }

                }
            }

            //reader.reset();

            System.out.printf("TreeSet contains %d elements \n", CSVtab.size());
           // System.out.println(word);

            //writer.write("TreeSet contains %d elements \n", CSVtab.size());




//            CSVtab.entrySet().stream()
//                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
//                    .forEach(System.out::println); // или любой другой конечный метод


            System.setOut(new PrintStream (new FileOutputStream ( "NewOut.txt" )));

            CSVtab.entrySet().stream()
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .forEach((k)->System.out.println( k.getKey() + ";"+k.getValue() ));

//            for (Map.Entry<String, Integer> entry : CSVtab.entrySet()) {
//               writer.write( entry.getKey() + " count = " + entry.getValue());
//                writer.write("\n");
//            }

            writer.close();
//read the data here
        } catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        } finally {
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }
    }
}
